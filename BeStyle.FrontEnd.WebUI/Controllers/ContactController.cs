﻿using System;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using Antlr.Runtime;
using BeStyle.Entities;
using BeStyle.FrontEnd.WebUI.Code.EmailManager;
using BeStyle.FrontEnd.WebUI.Models.AccountModels;
using BeStyle.FrontEnd.WebUI.Resources;
using BeStyle.Repositories.Abstract;
using BeStyle.WebUI.Security;

namespace BeStyle.FrontEnd.WebUI.Controllers
{
    public class ContactController : Controller
    {
        #region Private fields

        private readonly ISecurityManager _securityManager;
        private readonly IEmailManager _emailManager;

        #endregion

        #region Constructors

        public ContactController(ISecurityManager securityManager, IEmailManager emailManager)
        {
            this._securityManager = securityManager;
            this._emailManager = emailManager;
        }

        #endregion

        #region Web Actions
        
        [HttpGet]
        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendEmailtoUs(string subject, string text)
        {
            if (_securityManager.IsAuthenticated)
            {
                var userName = _securityManager.User.UserName;
                _emailManager.SendMailToUs(subject, text, userName);                
                return Json(new { Message = "You message successfully send to us! Wait and we'll contact with you!", Status = true });
            }
            else
            {
                return Json(new { Message = "You should log in before sending E-Mail!", Status = false });
            }
        }

        #endregion
    }
}