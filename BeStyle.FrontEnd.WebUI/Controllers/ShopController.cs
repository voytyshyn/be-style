﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BeStyle.Entities;
using BeStyle.FrontEnd.WebUI.Models;
using BeStyle.Repositories.Abstract;
using BeStyle.WebUI.Security;


namespace BeStyle.FrontEnd.WebUI.Controllers
{
    public class ShopController : Controller
    {
        #region Private Fields

        private readonly IProductRepository _productRepository;
        private readonly ISizeRepository _sizeRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly ISecurityManager _securityManager;
        private readonly ICommentRepository _commentRepository;

        #endregion

        #region Constructors

        public ShopController(IProductRepository productRepository, ISizeRepository sizeRepository, ICategoryRepository categoryRepository, ISecurityManager securityManager, ICommentRepository commentRepository)
        {
            this._productRepository = productRepository;
            this._categoryRepository = categoryRepository;
            this._sizeRepository = sizeRepository;
            this._securityManager = securityManager;
            this._commentRepository = commentRepository;
        }

        #endregion

        #region WebActions

        [HttpGet]
        public ActionResult Shop()
        {
            ShopModel products = new ShopModel
            {
                Products = _productRepository.GetProductsAll(),
                TopProducts = _productRepository.GetTopProducts(10), 
                Sizes = _sizeRepository.GetSizes(), 
                Categories = _categoryRepository.GetAllCategory(),
                Comments=_commentRepository.GetComments()                
            };
            products.MinPrice = 0;
            products.MaxPrice = products.Products.Count != 0 ? products.Products.Max(p => p.Price) : 0;

            return View(products);
        }

        [HttpGet]
        public ActionResult ShopByCategory(int? categoryId)
        {
            ShopModel products = new ShopModel();
            products.Products = categoryId.HasValue
                    ? _productRepository.GetProductsByCategoryId(categoryId.Value)
                    : _productRepository.GetProductsAll();
            products.TopProducts = _productRepository.GetTopProducts(10);
            products.Sizes = _sizeRepository.GetSizes();
            products.Categories = _categoryRepository.GetAllCategory();
            products.Comments = _commentRepository.GetComments();            
            products.MinPrice = 0;
            products.MaxPrice = products.Products.Count != 0 ? products.Products.Max(p => p.Price) : 0;
            
            return View("Shop", products);
        }

        [HttpGet]
        public ActionResult ShopByPrice(int start, int end, int? categoryId)
        {
            IList<ProductEntity> products = categoryId.HasValue
                ? _productRepository.GetProductsByPriceAndCategories(start, end, categoryId.Value)
                : _productRepository.GetProductsByPrice(start, end);

            return PartialView("_ProductListPartial", products);
        }

        [HttpPost]
        public ActionResult AddComment(int productId, string comment)
        {
            int userId=_securityManager.User.UserId;
            _commentRepository.SetComment(userId, productId, comment);
            DateTime dt=DateTime.Now;
            string user=_securityManager.User.UserName;
            var result = new { User = user, Comment = comment, Time = dt.ToLongTimeString() };
            return Json(result);
        }

        [ChildActionOnly]
        public ActionResult HeaderBottom()
        {
            var category = _categoryRepository.GetAllCategory();
            return PartialView("_HeaderBottom", category);
        }
        
        [HttpGet]
        public ActionResult ProductPartial(int id)
        {
            var model = new ProductModel()
            {
                Product = _productRepository.GetProductById(id),
                Comments = _commentRepository.GetCommentsForProduct(id),
                Sizes = _sizeRepository.GetSizes(),
                IsAuthenticated = _securityManager.IsAuthenticated
            };

            Session["Product"] = _productRepository.GetProductById(id);

            return PartialView("_ProductPartial", model);
        }

        #endregion
    }
}