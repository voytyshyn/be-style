﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeStyle.FrontEnd.WebUI.Models;
using BeStyle.FrontEnd.WebUI.Models.AccountModels;
using BeStyle.WebUI.Security;


namespace BeStyle.FrontEnd.WebUI.Controllers
{
    public class SecurityController : Controller
    {
        private readonly ISecurityManager _securityManager;

        public SecurityController(ISecurityManager securityManager)
        {
            this._securityManager = securityManager;
        }

        [ChildActionOnly]
        public ActionResult Security()
        {
            var secure = new SecurityModel();

            secure.IsAuthenticated = _securityManager.IsAuthenticated;
            secure.UserName = _securityManager.User.Login;
            secure.UserId = _securityManager.User.UserId;
            
            return PartialView("_Security", secure);
        }
    }
}