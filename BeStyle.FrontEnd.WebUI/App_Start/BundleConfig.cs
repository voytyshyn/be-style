﻿using System.Web.Optimization;

namespace BeStyle.FrontEnd.WebUI
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/styles").Include(
                      "~/Content/bootstrap.css",
                      "~/App_Themes/Default/css/font-awesome.min.css",
                      "~/App_Themes/Default/css/Styles.css",
                      "~/App_Themes/Default/css/main.css",
                      "~/App_Themes/Default/css/responsive.css",
                      "~/App_Themes/Default/css/animate.css",
                      "~/App_Themes/Default/Style/Site.css",
                      "~/Content/toastr.css",
                      "~/Content/site.css",
                      "~/Content/themes/base/*.css",
                      "~/Content/hover-styles.css"));


            #region Scripts bundles

            #region Validation sctipts

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(                
            "~/Scripts/Libs/JQuery/jquery.validate*"));

            #endregion

            #region Common Scripts

            bundles.Add(new ScriptBundle("~/bundles/common/scripts").Include(
                "~/Scripts/Libs/JQuery/jquery-2.1.1.js",
                "~/Scripts/Libs/JQuery/jquery-migrate-1.2.1.js",
                "~/Scripts/Libs/JQuery/jquery-ui.min-1.11.1.js",
                "~/Scripts/Libs/JQuery/jquery.validate.min.js",
                "~/Scripts/Libs/JQuery/jquery.prettyPhoto.js",
                "~/Scripts/Libs/JQuery/jquery.scrollUp.min.js",
                "~/Scripts/Libs/Bootstrap/bootstrap.js",
                "~/Scripts/Libs/Custom/main.js",
                "~/Scripts/Libs/JQuery/jquery.unobtrusive-ajax.js",
                "~/Scripts/Libs/Custom/loading.js",
                "~/Scripts/Libs/Toastr/toastr.min.js"));

            #endregion

            #region Shop

            bundles.Add(new ScriptBundle("~/bundles/Shop/scripts").Include(
                        "~/Scripts/Pages/shop.js",
                        "~/Scripts/Pages/cart.js",
                        "~/Scripts/Pages/constructor.js"));

            #endregion

            #region Constructor

            bundles.Add(new ScriptBundle("~/bundles/Constructor/scripts").Include(
                        "~/Scripts/Pages/constructor.js",
                        "~/Scripts/Pages/cart.js",
                        "~/Scripts/Libs/Custom/loading.js"));

            #endregion

            #region Cart

            bundles.Add(new ScriptBundle("~/bundles/Cart/scripts").Include(
                        "~/Scripts/Pages/cart.js"));

            #endregion

            #region Profile

            bundles.Add(new ScriptBundle("~/bundles/Profile/scripts").Include(
                "~/Scripts/Pages/profile.js"));

            #endregion

            #region Login

            bundles.Add(new ScriptBundle("~/bundles/Login/scripts").Include(
                "~/Scripts/Pages/login.js"));

            #endregion

            #endregion

            BundleTable.EnableOptimizations = false;
        }
    }
}