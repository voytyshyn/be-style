﻿(function ($) {
    var CartPage = function () {

        var initializeEvents = function () {
            $(".RemoveLink").click(function () {
                var tr = $(this).closest('tr');
                tr.remove();
                var recordToDelete = $(this).attr("data-id");
                var recordSize = $(this).attr("data-size-id");
                if (recordToDelete != '') {
                    $.post("/Cart/RemoveFromCart", { "productId": recordToDelete, "sizeId": recordSize },
                        function (data) {
                            var tr = $(this).closest('tr');
                            tr.remove();
                            toastr["warning"](data.Message, "");
                        });
                }
            });

            $(".Submit").click(function () {
                showLoader();
                $.ajax({
                    type: 'POST',
                    url: '/Cart/SubmitOrder',
                    success: function (data) {
                        hideLoader();
                        if (data.Status == true) {
                            toastr["success"](data.Message, "Success!")
                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-full-width",
                                "preventDuplicates": false,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                        }
                        else {
                            toastr["warning"](data.Message, "Warning");
                            var returnUrl = window.location.pathname;
                            window.location.href = "/Login?returnUrl=" + returnUrl;
                        }
                    }
                });
            });

            window.setSize = function (size) {
                var field = document.getElementById("data-for-add");
                var dat = field.getAttribute("data-size-id");
                field.setAttribute("data-size-id", size);
            }
        };

        this.initialize = function () {
            initializeEvents();
        };
    };

    $(document).ready(function () {
        var page = new CartPage();
        page.initialize();
    });
})(window.jQuery);

