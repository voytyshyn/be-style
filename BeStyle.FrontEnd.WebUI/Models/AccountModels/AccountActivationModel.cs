﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeStyle.FrontEnd.WebUI.Models.AccountModels
{
    public class AccountActivationModel
    {
        public bool IsActivated { get; set; }
    }
}