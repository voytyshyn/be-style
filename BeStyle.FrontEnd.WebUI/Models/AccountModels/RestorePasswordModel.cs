﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BeStyle.FrontEnd.WebUI.Models.AccountModels
{
    public class RestorePasswordModel
    {
        [Display(Name = "Email")]
        [RegularExpression(@"[A-Za-z0-9_-]+@[A-Za-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$",
            ErrorMessage = "Email is invalid")]
        [Required(ErrorMessage = "Email field can't be empty")]
        public string Email { get; set; }
    }
}