﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeStyle.FrontEnd.WebUI.Models.AccountModels
{
    public class AccountModel
    {
        public Login Login { get; set; }

        public Registration Registration { get; set; }
    }
}