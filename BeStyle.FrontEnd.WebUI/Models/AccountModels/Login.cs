﻿using System.ComponentModel.DataAnnotations;

namespace BeStyle.FrontEnd.WebUI.Models.AccountModels
{
    public class Login
    {
        [Required(ErrorMessage = "User name can't be empty")]
        [Display(Name = "Login or email")]
        public string UserName { get; set; }

        [RegularExpression(@"[./()@=\-?\*,\w]{5,}", ErrorMessage = "Password is invalid or too short")]
        [Required(ErrorMessage = "Password can't be empty")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}