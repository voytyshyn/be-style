﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace BeStyle.FrontEnd.WebUI.Models.AccountModels
{
    public class Registration
    {
        [RegularExpression(@"[A-Za-zА-Яа-яІЇёЁЄє-]{2,}", ErrorMessage = "First name is invalid")]
        [Required(ErrorMessage = "First name field can't be empty")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [RegularExpression("[A-Za-zА-Яа-яІЇёЁЄє-]{2,}", ErrorMessage = "Last name is invalid")]
        [Required(ErrorMessage = "Last name field can't be empty")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [RegularExpression(@"[A-Za-z0-9_-]+@[A-Za-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$",
            ErrorMessage = "Email is invalid")]
        [Required(ErrorMessage = "Email field can't be empty")]
        [Display(Name = "Email adress")]
        public string Email { get; set; }

        [Display(Name = @"Phone in format (xxx) xxx xxxx")]
        [RegularExpression(@"^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$",
            ErrorMessage = "Phone is invalid. Use next format: (xxx) xxx xxxx")]
        public string Phone { get; set; }

        [RegularExpression(@"[\w]{5,50}", ErrorMessage = "Login is invalid or too short.")]
        [Required(ErrorMessage = "Login field can't be empty")]
        [Display(Name = "Login")]
        public string Login { get; set; }

        [RegularExpression(@"[./()@=\-?\*,\w]{5,}", ErrorMessage = "Password is invalid or too short")]
        [Required(ErrorMessage = "Password field can't be empty")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [RegularExpression(@"[./()@=\-?\*,\w]{5,}", ErrorMessage = "Password is invalid or too short")]
        [Required(ErrorMessage = "Password confirmation field can't be empty")]
        [Display(Name = "Confirm Password")]
        public string PasswordConfirmation { get; set; }

        public Guid ActivationCode { get; set; }
    }
}