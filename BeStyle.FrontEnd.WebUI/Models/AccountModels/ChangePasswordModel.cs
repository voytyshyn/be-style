﻿using System.ComponentModel.DataAnnotations;

namespace BeStyle.FrontEnd.WebUI.Models.AccountModels
{
    public class ChangePasswordModel
    {
        [RegularExpression(@"[\w./()@=\-?\*,]{5,}", ErrorMessage = "Password is invalid or too short")]
        [Required(ErrorMessage = "Password field can't be empty")]
        [Display(Name = "Old password")]
        public string Password { get; set; }

        [RegularExpression(@"[\w./()@=\-?\*,]{5,}", ErrorMessage = "Password is invalid or too short")]
        [Required(ErrorMessage = "Password confirmation field can't be empty")]
        [Display(Name = "Confirm Password")]
        public string PasswordConfirmation { get; set; }

        [RegularExpression(@"[\w./()@=\-?\*,]{5,}", ErrorMessage = "Password is invalid or too short")]
        [Required(ErrorMessage = "New password field can't be empty")]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        public int UserId { get; set; }
    }
}