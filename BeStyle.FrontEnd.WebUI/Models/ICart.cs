﻿using BeStyle.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeStyle.FrontEnd.WebUI.Models
{
    public interface ICart
    {
        void AddItem(ProductEntity product, SizeEntity size, LogoEntity logoFront, int? logoFrontScale, LogoEntity logoBack, int? logoBackScale, int quantity);
        void RemoveItem(int productId, int sizeId);
        decimal CalcTotalPrice();
        void Clear();
        IEnumerable<CartItem> Items { get; }
    }
}