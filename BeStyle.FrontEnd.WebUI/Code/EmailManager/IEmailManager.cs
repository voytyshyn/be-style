﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeStyle.Entities;

namespace BeStyle.FrontEnd.WebUI.Code.EmailManager
{
    public interface IEmailManager
    {
        bool SendActivationCode(string to, string name, string login, Guid code);

        bool SendRestoredPassword(string to, string name, string restoredPassword);

        bool SendMailToUs(string subject, string text, string fromDisplay);
    }
}
