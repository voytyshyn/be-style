﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Repositories.Enums
{
    public enum UpdateUserInfo
    {
        Success,
        LoginIsBusy,
        EmailIsBusy,
        LoginAndEmailIsBusy
    }
}
