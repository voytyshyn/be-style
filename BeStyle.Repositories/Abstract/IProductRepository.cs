﻿using System.Collections.Generic;
using BeStyle.Entities;
using System.Data;

namespace BeStyle.Repositories.Abstract
{
    public interface IProductRepository
    {
        IList<ProductEntity> GetTopProducts(int count);
        List<ProductEntity> GetProductsAll();
        ProductEntity GetProductById(int id);        
        void DeleteProduct(int productId);
        void CreateProduct(ProductEntity product);        
        CategoryEntity GetCategoryForProductById(int id);
        List<ProductEntity> GetProductsByCategoryId(int categoryId);
        List<ProductEntity> GetProductsByPrice(int start, int end);
        List<ProductEntity> GetProductsByPriceAndCategories(int start, int end, int categoryId);
    }
}
