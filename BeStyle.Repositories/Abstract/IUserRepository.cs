﻿using System.Collections.Generic;
using BeStyle.Entities;
using BeStyle.Repositories.Enums;

namespace BeStyle.Repositories.Abstract
{
    public interface IUserRepository
    {
        void ActivateUser(int id);
        List<UserEntity> GetUsersAll();       
        List<string> GetAllRolesValueForUser(int userId);
        void UpdateUserInfo(int Id, string FirstName, string LastName, string Email, string Phone, string Login,
            string Password, bool IsMaster, bool IsModerator, bool IsEditor, bool IsUser, bool IsDisabled);
        void UpdateUserInfo(UserEntity user);
        UpdateUserInfo UpdateMainUserInfo(UserEntity user);
        void DeleteUser(int userId);
        bool CreateUser(UserEntity user);
        UserEntity GetUserByLogin(string login);
        UserEntity GetUserByEmail(string email);
        UserEntity GetUserById(int id);
        void ChangePassword(int id, string password);
        void RestorePassword(int id, string password);
    }
}
