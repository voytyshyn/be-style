﻿using BeStyle.Entities;
using BeStyle.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Repositories.Sql
{
    public class CommentRepository:ICommentRepository
    {
        #region Private Fields

        private readonly string _connectionString;
        private const string GET_COMMENTS_FOR_PRODUCT = @"SELECT c.Id, c.UserId, c.ProductId, c.Comment, c.CommentTime, u.FirstName
                                                     FROM [tblComment] AS c 
                                                     INNER JOIN [tblUser] AS u
                                                     ON c.UserId=u.Id
                                                     WHERE ProductId=@ProductId ORDER BY c.CommentTime DESC";
        private const string GET_COMMENTS = "SELECT * FROM [tblComment]"; 
        private const string INSERT_COMMENT_FOR_PRODUCT = "INSERT INTO [tblComment] ([UserId], [ProductId], [Comment], [CommentTime]) VALUES(@UserId, @ProductId, @Comment, @CommentTime)";

        #endregion

        #region Constructors

        public CommentRepository(string connectionString)
        {
            _connectionString=connectionString;
        }

        #endregion

        #region ICommentRepository

        public List<CommentEntity> GetComments()
        {
            List<CommentEntity> comments = new List<CommentEntity>();
            using(SqlConnection connection=new SqlConnection(_connectionString))
            {
                connection.Open();
                using(SqlCommand cmd=new SqlCommand(GET_COMMENTS, connection))
                {
                    using (SqlDataReader reader=cmd.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            comments.Add(ReadComment(reader));
                        }
                    }
                }
                return comments;
            }
        }

        public List<CommentEntity> GetCommentsForProduct(int productId)
        {
            List<CommentEntity> comments = new List<CommentEntity>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_COMMENTS_FOR_PRODUCT, connection))
                {
                    cmd.Parameters.AddWithValue("ProductId", productId);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            comments.Add(ReadCommentWithName(reader));
                        }
                    }
                }
                return comments;
            }
        }

        public void SetComment(int userId, int productId, string comment)
        {
            using(SqlConnection connection=new SqlConnection(_connectionString))
            {
                connection.Open();
                using(SqlCommand cmd=new SqlCommand(INSERT_COMMENT_FOR_PRODUCT, connection))
                {
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    cmd.Parameters.AddWithValue("@ProductId", productId);
                    cmd.Parameters.AddWithValue("@Comment", comment);
                    cmd.Parameters.AddWithValue("@CommentTime", DateTime.Now);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region Helpers

        private CommentEntity ReadCommentWithName(SqlDataReader reader)
        {
            CommentEntity comment = new CommentEntity();
            comment.Id = (int)reader["Id"];
            comment.ProductId = (int)reader["ProductId"];
            comment.UserId = (int)reader["UserId"];
            comment.Comment = (string)reader["Comment"];
            comment.CommentTime = (DateTime)reader["CommentTime"];            
            comment.UserName = (string)reader["FirstName"];           
            return comment;
        }

        private CommentEntity ReadComment(SqlDataReader reader)
        {
            CommentEntity comment = new CommentEntity();
            comment.Id = (int)reader["Id"];
            comment.ProductId = (int)reader["ProductId"];
            comment.UserId = (int)reader["UserId"];
            comment.Comment = (string)reader["Comment"];
            comment.CommentTime = (DateTime)reader["CommentTime"];         
            return comment;
        }

        #endregion
    }
}
