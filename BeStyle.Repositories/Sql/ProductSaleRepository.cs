﻿using BeStyle.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using BeStyle.Repositories.Abstract;

namespace BeStyle.Repositories.Sql
{
    public class ProductSaleRepository : IProductSaleRepository
    {
        #region Private Fields

        private readonly string _connectionString;     
        private const string GET_TOP_PRODUCT = "SELECT TOP (@Count) [ProductId] FROM [tblProductSale] ORDER BY [SaleQuantity] DESC";

        #endregion

        #region Constructors

        public ProductSaleRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        #endregion

        #region IProductSaleRepository

        public IList<int> GetTop(int count)
        {
            IList<int>productId= new List<int>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_TOP_PRODUCT, connection))
                {
                    cmd.Parameters.AddWithValue("Count", count);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        int i = 0;
                        while (reader.Read())
                        {
                            if (i >= count)
                            {
                                return productId;
                            }
                            productId.Add((int)reader["ProductId"]);
                            i++;
                        }
                    }
                    return productId;
                }
            }
        }

        #endregion
    }
}
