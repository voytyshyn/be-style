﻿using BeStyle.Entities;
using BeStyle.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;

namespace BeStyle.Repositories.Sql
{
    public class ProductRepository : IProductRepository
    {
        #region Queryies
        
        private const string GET_ALL_QUERY = "SELECT [Id],[Name],[Price],[CategoryId], [Picture], [PictureBack],[Description] FROM [tblProduct]";
        private const string GET_ALL_BY_CATEGORY = "SELECT [Id],[Name],[Price],[CategoryId],[Picture], [PictureBack], [Description] FROM [tblProduct] WHERE [CategoryId]=@Id";
        private const string DELETE_BY_ID_QUERY = "DELETE FROM [tblProduct] WHERE [Id]=@ProductId";
        private const string INSERT_QUERY = "INSERT INTO [tblProduct]([Name],[CategoryId],[Price],[Picture], [PictureBack], [Description]) VALUES(@Name, @CategoryId, @Price, @Picture, @PictureBack, @Description)";
        private const string UPDATE_QUERY = "UPDATE [tblProduct] SET [Name]=@Name, [CategoryId]=@CategoryId, [Price]=@Price, [Picture]=@Picture, [PictureBack]=@PictureBack WHERE [Id]=@Id";
        private const string GET_BY_ID_QUERY = "SELECT [Id],[Name],[Price],[CategoryId], [Picture], [PictureBack],[Description] FROM [tblProduct] WHERE [Id]=@Id";
        private const string GET_CATEGORY_FOR_PRODUCT_QUERY =@"SELECT category.Category 
                                                             FROM [tblProduct] product
                                                             INNER JOIN [tblCategory] category
                                                             WHERE product.Id=@Id";

        private const string GET_PRODUCTS_BY_PRICE =
            @"SELECT [Id],[Name],[Price],[CategoryId],[Picture], [PictureBack], [Description] FROM [tblProduct] WHERE ([Price] BETWEEN @Start AND @End) ORDER BY [Price]";
        private const string GET_PRODUCTS_BY_PRICE_AND_CATEGORY =
            @"SELECT [Id],[Name],[Price],[CategoryId],[Picture], [PictureBack], [Description] FROM [tblProduct] WHERE (([Price] BETWEEN @Start AND @End) AND [CategoryId]=@CategoryId) ORDER BY [Price]";


        #endregion

        #region Fields

        private readonly string _connectionString;

        #endregion

        #region Constructors

        public ProductRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        #endregion

        #region IProductRepository

        public IList<ProductEntity> GetTopProducts(int count)
        {
            ProductSaleRepository prSale = new ProductSaleRepository(_connectionString);
            IList<int> productId = prSale.GetTop(count);
            List<ProductEntity> products = new List<ProductEntity>();
            foreach (var i in productId)
            {
                products.Add(GetProductById(i));
            }
            return products;            
        }

        public List<ProductEntity> GetProductsAll()
        {
            List<ProductEntity> products = new List<ProductEntity>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_ALL_QUERY, connection))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ProductEntity product = ReadProduct(reader);
                            products.Add(product);
                        }
                        return products;
                    }
                }
            }
        }

        public void UpdateProduct(int id, string name, string price, string categoryId, byte[] img, byte [] imgBack)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(UPDATE_QUERY, connection))
                {
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = name;
                    cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = Convert.ToInt32(categoryId);
                    cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = Convert.ToDecimal(price);
                    cmd.Parameters.Add("@Picture", SqlDbType.VarBinary).Value = img;
                    cmd.Parameters.Add("@PictureBack", SqlDbType.VarBinary).Value = imgBack;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteProduct(int Id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand(DELETE_BY_ID_QUERY, connection))
                {
                    cmd.Parameters.AddWithValue("ProductId", Id);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void CreateProduct(ProductEntity product)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(INSERT_QUERY, connection))
                {
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = product.Name;
                    cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = product.CategoryId;
                    cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = product.Price;
                    cmd.Parameters.Add("@Picture", SqlDbType.VarBinary).Value = product.Picture;
                    cmd.Parameters.Add("@PictureBack", SqlDbType.VarBinary).Value = product.PictureBack;
                    cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = product.Description;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public ProductEntity GetProductById(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand(GET_BY_ID_QUERY, connection))
                {
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return ReadProduct(reader);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }

        public List<ProductEntity> GetProductsByCategoryId(int categoryId)
        {
            List<ProductEntity> products = new List<ProductEntity>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_ALL_BY_CATEGORY, connection))
                {
                    cmd.Parameters.AddWithValue("Id", categoryId);
                    using(SqlDataReader reader=cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        ProductEntity product = ReadProduct(reader);
                        products.Add(product);
                    }
                    return products;
                }
            }            
        }

        public CategoryEntity GetCategoryForProductById(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_CATEGORY_FOR_PRODUCT_QUERY, connection))
                {
                    cmd.Parameters.AddWithValue("Id", id);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        return ReadCategory(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        public List<ProductEntity> GetProductsByPrice(int start, int end)
        {
            List<ProductEntity> products = new List<ProductEntity>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_PRODUCTS_BY_PRICE, connection))
                {
                    cmd.Parameters.AddWithValue("Start", start);
                    cmd.Parameters.AddWithValue("End", end);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ProductEntity product = ReadProduct(reader);
                            products.Add(product);
                        }
                    }
                    return products;
                }
            }            
        }

        public List<ProductEntity> GetProductsByPriceAndCategories(int start, int end, int categoryId)
        {
            List<ProductEntity> products = new List<ProductEntity>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_PRODUCTS_BY_PRICE_AND_CATEGORY, connection))
                {
                    cmd.Parameters.AddWithValue("Start", start);
                    cmd.Parameters.AddWithValue("End", end);
                    cmd.Parameters.AddWithValue("CategoryId", categoryId);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ProductEntity product = ReadProduct(reader);
                            products.Add(product);
                        }
                    }
                    return products;
                }
            }    
        }


        #endregion

        #region Helpers
        private ProductEntity ReadProduct(SqlDataReader reader)
        {
            ProductEntity product = new ProductEntity();
            product.Id = (int)reader["Id"];
            product.Name = (string)reader["Name"];
            product.Price = (decimal)reader["Price"];
            product.Picture = (byte[])reader["Picture"];
            product.PictureBack = (byte[])reader["PictureBack"];
            product.CategoryId = (int)reader["CategoryId"];
            product.Description = (reader["Description"] == DBNull.Value) ? string.Empty : (string)reader["Description"];       
            return product;
        }

        private CategoryEntity ReadCategory(SqlDataReader reader)
        {
            CategoryEntity category = new CategoryEntity();
            category.Id = (int)reader["Id"];
            category.Category = (string)reader["Category"];
            return category;
        }
        #endregion
    }
}
