﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Entities
{
    public class SizeEntity
    {
        public int Id { get; set; }
        public string Size { get; set; }
    }
}
