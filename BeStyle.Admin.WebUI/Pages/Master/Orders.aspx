﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Orders.aspx.cs" Inherits="BeStyle.Admin.WebUI.Pages.Master.Orders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <div class="center" style="padding: 10px;">
        <div class="grid-orders">
            <h3 style="color: gray; font-family: Arial;">Orders Table</h3>
            <asp:Label runat="server" Text="Search by user:"></asp:Label>
            <asp:DropDownList ID="ddlUser" runat="server" OnLoad="User_OnLoad" AppendDataBoundItems="True" AutoPostBack="true" OnSelectedIndexChanged="ddlUser_SelectedIndexChanged">
                <asp:ListItem Text="All" Value ="0" Selected="True"></asp:ListItem>
            </asp:DropDownList>
            <asp:GridView ID="gvOrders" runat="server" DataKeyNames="Id" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None"
                BorderWidth="1px" CellPadding="3" DataSourceID="obds_Orders" ShowFooter="True" AutoGenerateColumns="False"
                AutoGenerateSelectButton="True" AutoGenerateEditButton="True" OnRowUpdating="gvOrders_OnRowUpdating"
                SelectedIndex="1" OnSelectedIndexChanged="gvOrders_SelectedIndexChanged">
                <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                <RowStyle BackColor="White" ForeColor="#003399" />
                <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />

                <Columns>
                    <asp:TemplateField HeaderText="ID" SortExpression="ID">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lbl1" Text='<%#Bind("Id")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="DateTime" SortExpression="ID">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lbl2" Text='<%#Bind("Time")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="User" SortExpression="ID">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lbl3" Text='<%#Bind("User.Login")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="IsDone" SortExpression="ID">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Enabled="False" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' OnPreRender="CheckBox1_OnPreRender"/>
                        </EditItemTemplate>
                    </asp:TemplateField>
                </Columns>

                <SelectedRowStyle BackColor="LightCyan"
                    ForeColor="DarkBlue"
                    Font-Bold="true" />
            </asp:GridView>
        </div>

        <asp:ObjectDataSource ID="obds_Orders" runat="server" OnObjectCreating="obds_Orders_ObjectCreating" SelectMethod="GetAllSubmits" UpdateMethod="UpdateOrderStatus" OnUpdating="obds_Orders_OnUpdating"
            TypeName="BeStyle.Repositories.Sql.OrderRepository"></asp:ObjectDataSource>
        <br />

        <div class="grid-orderitem" style="padding-left: inherit;">
            <h3 style="color: gray; font-family: Arial;">Order Items Table</h3>
            <asp:GridView ID="gvOrderItems" runat="server" DataKeyNames="Id" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None"
                BorderWidth="1px" CellPadding="3" DataSourceID="obds_OrderItems" ShowFooter="True" AutoGenerateColumns="False" OnSelectedIndexChanged="gvOrderItems_SelectedIndexChanged">
                <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                <RowStyle BackColor="White" ForeColor="#003399" />
                <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />

                <Columns>
                    <asp:TemplateField HeaderText="Id" SortExpression="ID">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="ProductId" Text='<%#Bind("Id")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product" SortExpression="ID">
                        <%--                <ItemTemplate>
                    <asp:Label runat="server" ID="ProductId" Text='<%#Bind("ProductId")%>' />
                </ItemTemplate>--%>
                        <ItemTemplate>
                            <asp:Image ID="Picture" runat="server" ImageUrl='<%#GetProductImage((int)Eval("ProductId"))%>' CssClass="picture2" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Front Logo" SortExpression="ID">
                        <%--       <ItemTemplate>
                    <asp:Label runat="server" ID="LogoId" Text='<%#Bind("LogoId")%>' />
                </ItemTemplate>--%>
                        <ItemTemplate>
                            <asp:Image ID="Picture2" runat="server" ImageUrl='<%#GetLogoImage((int?)Eval("LogoFrontId"))%>' CssClass="picture2" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Back Logo" SortExpression="ID">
                        <%--       <ItemTemplate>
                    <asp:Label runat="server" ID="LogoId" Text='<%#Bind("LogoId")%>' />
                </ItemTemplate>--%>
                        <ItemTemplate>
                            <asp:Image ID="Picture2" runat="server" ImageUrl='<%#GetLogoImage((int?)Eval("LogoBackId"))%>' CssClass="picture2" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Size" SortExpression="ID">
                        <ItemTemplate>
                            <%--                            <asp:Label runat="server" ID="SizeId" Text='<%#Bind("SizeId")%>' />--%>
                            <asp:Label runat="server" ID="SizeId" Text='<%#GetSizeValue((int)Eval("SizeId"))%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quantity" SortExpression="ID">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="Quantity" Text='<%#Bind("Quantity")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="LogoFrontScale" SortExpression="ID">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="LogoScale" Text='<%#Bind("LogoFrontScale")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="LogoBackScale" SortExpression="ID">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="LogoScale" Text='<%#Bind("LogoBackScale")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>

        <asp:ObjectDataSource ID="obds_OrderItems" runat="server" OnObjectCreating="obds_OrderItems_ObjectCreating" SelectMethod="GetAllOrdersForSubmit" 
            TypeName="BeStyle.Repositories.Sql.OrderRepository" OnSelecting="obds_OrderItems_Selecting">
            <SelectParameters>
                <asp:Parameter Name="submitOrderId" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
</asp:Content>


