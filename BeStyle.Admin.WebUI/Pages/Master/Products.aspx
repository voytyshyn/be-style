﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" Theme="DefaultTheme" AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="BeStyle.Admin.WebUI.Pages.Master.Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/Scripts.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div>
        <div class="center">
            <asp:GridView ID="gvProducts" runat="server" DataKeyNames="Id" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None"
                BorderWidth="1px" CellPadding="3" DataSourceID="obsProducts" ShowFooter="True" AutoGenerateColumns="False">
                <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                <RowStyle BackColor="White" ForeColor="#003399" />
                <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />

                <Columns>

                    <asp:TemplateField HeaderText="ID" SortExpression="ID">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblId" Text='<%#Bind("Id")%>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnInsert" runat="server" Text="Add" OnClick="btnInsert_Click" ValidationGroup="ProductPageValidation" />
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Name" SortExpression="Name">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("Name") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBoxName" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqValNameEdit" runat="server" ErrorMessage="Name can't be empty"
                                ControlToValidate="TextBoxName" ValidationGroup="ProductPageValidation" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regValNameEdit" runat="server" Display="None" ErrorMessage="Name is invalid"
                                ValidationGroup="ProductPageValidation" ValidationExpression="[A-Za-zА-Яа-яІЇёЁЄє-]{2,}"
                                ControlToValidate="TextBoxName" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="tbxName" runat="server" Text='<%#Bind("Name") %>'>
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqValNameEdit" runat="server" ErrorMessage="Name can't be empty"
                                ControlToValidate="tbxName" ValidationGroup="ProductPageValidation" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regValNameEdit" runat="server" Display="None" ErrorMessage="Name is invalid"
                                ValidationGroup="ProductPageValidation" ValidationExpression="[A-Za-zА-Яа-яІЇёЁЄє-]{2,}"
                                ControlToValidate="tbxName" />
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Price" SortExpression="Price">
                        <ItemTemplate>
                            <asp:Label ID="Price" runat="server" Text='<%#Eval("Price") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="tbxPrice" runat="server" Text='<%# Bind("Price") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqValPriceEdit" runat="server" ErrorMessage="Price can't be empty"
                                ControlToValidate="tbxPrice" ValidationGroup="ProductPageValidation" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regValPriceEdit" runat="server" Display="None" ErrorMessage="Name is invalid"
                                ValidationGroup="ProductPageValidation" ValidationExpression="[0-9]{2,}"
                                ControlToValidate="tbxPrice" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="tbxPrice" runat="server" Text='<%#Bind("Price") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqValPriceEdit" runat="server" ErrorMessage="Price can't be empty"
                                ControlToValidate="tbxPrice" ValidationGroup="ProductPageValidation" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regValPriceEdit" runat="server" Display="None" ErrorMessage="Price is invalid"
                                ValidationGroup="ProductPageValidation" ValidationExpression="[0-9]{2,}"
                                ControlToValidate="tbxPrice" />
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Category">
                        <ItemTemplate>
                            <asp:Label ID="lblCategory" runat="server" Text='<%#GetCategory((int)Eval("CategoryId")) %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="Category" runat="server" OnLoad="Category_OnLoad"></asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCategory" runat="server" OnLoad="Category_OnLoad"></asp:DropDownList>
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Image ID="Picture" runat="server" ImageUrl='<%#GetImage(Eval("Picture"))%>' CssClass="picture2"
                                onclick="changeSizeImage(this)" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:FileUpload ID="FileUpload" runat="server" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:FileUpload ID="flupImage" runat="server" />
                        </FooterTemplate>
                    </asp:TemplateField>

                        <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Image ID="PictureBack" runat="server" ImageUrl='<%#GetImage(Eval("PictureBack"))%>' CssClass="picture2"
                                onclick="changeSizeImage(this)" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:FileUpload ID="FileUploadBack" runat="server" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:FileUpload ID="flupImageBack" runat="server" />
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Description" SortExpression="Description">
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBoxDescription" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="tbxDescription" runat="server" Text='<%#Bind("Description") %>'>
                            </asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>

                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#00547E" />

            </asp:GridView>

            <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowSummary="True" ForeColor="Red" ValidationGroup="ProductPageValidation" />
            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowSummary="True" ForeColor="Red" ValidationGroup="ProductPageValidation" />
            <br />
            <br />

            <asp:ObjectDataSource ID="obsProducts" runat="server" OnObjectCreating="obsProducts_ObjectCreating" SelectMethod="GetProductsAll"
                TypeName="BeStyle.Repositories.Sql.ProductRepository" DeleteMethod="DeleteProduct">
                <DeleteParameters>
                    <asp:Parameter Name="Id" Type="Int32" />
                </DeleteParameters>
            </asp:ObjectDataSource>
        </div>
    </div>
</asp:Content>
