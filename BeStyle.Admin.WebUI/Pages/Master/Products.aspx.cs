﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Configuration;
using BeStyle.Repositories.Sql;
using System.IO;
using System.Web.UI.WebControls;
using BeStyle.Entities;
using System.Drawing.Imaging;
using Microsoft.Practices.Unity;
using BeStyle.Repositories.Abstract;

namespace BeStyle.Admin.WebUI.Pages.Master
{
    public partial class Products : System.Web.UI.Page
    {
        [Dependency]
        public ICategoryRepository categoryRepository { get; set; }
        [Dependency]
        public IProductRepository productRepository { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void obsProducts_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = productRepository;
        }


        public string GetImage(object img)
        {
            {
                return "data:image/png;base64," + Convert.ToBase64String((byte[])img);
            }
        }

        public string GetCategory(int id)
        {            
            return categoryRepository.GetCategoryValueById(id);
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            ProductEntity product = new ProductEntity();
            product.Name = ((TextBox)gvProducts.FooterRow.FindControl("tbxName")).Text;
            product.Price = Convert.ToDecimal(((TextBox)gvProducts.FooterRow.FindControl("tbxPrice")).Text);
            product.CategoryId = Convert.ToInt32(((DropDownList)gvProducts.FooterRow.FindControl("ddlCategory")).SelectedValue);
            product.Description = ((TextBox)gvProducts.FooterRow.FindControl("tbxDescription")).Text;
            FileUpload flup = (FileUpload)gvProducts.FooterRow.FindControl("flupImage");
            if (flup.HasFile)
            {
                byte[] productImage = flup.FileBytes;
                product.Picture = productImage;
            }
            FileUpload flupBack = (FileUpload)gvProducts.FooterRow.FindControl("flupImageBack");
            if (flupBack.HasFile)
            {
                byte[] productImage = flupBack.FileBytes;
                product.PictureBack = productImage;
            }            
            productRepository.CreateProduct(product);
            gvProducts.DataBind();
        }

        protected void Category_OnLoad(object sender, EventArgs e)
        {
            var ddl = (DropDownList)sender;
            var index = ddl.SelectedIndex;            
            ddl.DataSource = categoryRepository.GetAllCategory();
            ddl.DataValueField = @"Id";
            ddl.DataTextField = @"Category";
            if (!Page.IsPostBack)
            {
                ddl.DataBind();
            }
        }
    }
}

