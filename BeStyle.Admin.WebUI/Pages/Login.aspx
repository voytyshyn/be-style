﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LoginMasterPage.Master" Theme="DefaultTheme" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="BeStyle.Admin.WebUI.Pages.Login" StylesheetTheme="DefaultTheme" %>

<asp:Content ID="ctnHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ctnBody" ContentPlaceHolderID="main" runat="server">
    <div id="log" class="parent">
        <a href="#" class="logotypeInLogin"></a>
        <asp:Login ID="lgnLogin" runat="server" BorderColor="#ECFFE7" BorderPadding="30" BorderStyle="Dashed" ForeColor="#0000CC" Font-Size="16pt" 
                   DisplayRememberMe="True" CssClass="log" OnLoggingIn="lgnLogin_OnLoggingIn">
            <LayoutTemplate>
                <table cellpadding="5" cellspacing="0" style="border-collapse:collapse;">
                    <tr>
                        <td>
                            <table cellpadding="0">
                                <tr>
                                    <td align="center" colspan="2">Login</td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Admin Name:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="UserName" runat="server" CssClass="login-input login-background" placeholder="Username or Email"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="lgnLogin">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Password" CssClass="login-input password-background" runat="server" TextMode="Password" placeholder="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="lgnLogin">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="RememberMe" runat="server" Text="Remember me next time?" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" style="color:Red;">
                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="LoginButton" CssClass="btn" runat="server" CommandName="Login" Text="Log In" ValidationGroup="lgnLogin" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
        </asp:Login>
    </div>  
</asp:Content>
