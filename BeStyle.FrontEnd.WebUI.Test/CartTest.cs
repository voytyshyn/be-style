﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BeStyle.FrontEnd.WebUI.Models;
using BeStyle.Entities;

namespace BeStyle.FrontEnd.WebUI.Test
{
    [TestClass]
    public class CartTest
    {
        [TestMethod]
        public void Test_AddNewItem()
        {
            CartItem item1 = new CartItem
            {
                Product = new ProductEntity { Id = 1, Name = "Name1" },
                LogoFront = new LogoEntity { Id = 1 },
                Size = new SizeEntity { Id = 1, Size = "S" }
            };
            CartItem item2 = new CartItem
            {
                Product = new ProductEntity { Id = 2, Name = "Name2" },
                LogoFront = new LogoEntity { Id = 2 },
                Size = new SizeEntity { Id = 2, Size = "L" }
            };

            Cart cart = new Cart();            
            cart.AddItem(item1.Product, item1.Size, item1.LogoFront, 0, null, 0, 1);
            cart.AddItem(item2.Product, item1.Size, item1.LogoFront, 0, null, 0, 1);
            cart.AddItem(item1.Product, item1.Size, item1.LogoFront, 0, null, 0, 1);

            CartItem[] results = cart.Items.ToArray();
            Assert.AreEqual(results.Length, 2);
            Assert.AreEqual(results[0].Product, item1.Product);
            Assert.AreEqual(results[1].Product, item2.Product);
        }

        [TestMethod]
        public void Test_AddExistItemQuantity()
        {
            CartItem item1 = new CartItem
            {
                Product = new ProductEntity { Id = 1, Name = "Name1" },
                LogoFront = new LogoEntity { Id = 1 },
                Size = new SizeEntity { Id = 1, Size = "S" }
            };
            CartItem item2 = new CartItem
            {
                Product = new ProductEntity { Id = 2, Name = "Name2" },
                LogoFront = new LogoEntity { Id = 2 },
                Size = new SizeEntity { Id = 2, Size = "L" }
            };
            Cart cart = new Cart();
            cart.AddItem(item1.Product, item1.Size, item1.LogoFront, 0, null, 0, 1);
            cart.AddItem(item2.Product, item1.Size, item1.LogoFront, 0, null, 0, 1);
            cart.AddItem(item1.Product, item1.Size, item1.LogoFront, 0, null, 0, 5);
            CartItem[] results = cart.Items.ToArray();
            Assert.AreEqual(results.Length, 2);
            Assert.AreEqual(results[0].Quantity, 6);
            Assert.AreEqual(results[1].Quantity, 1);
        }

        [TestMethod]
        public void Test_RemoveItem()
        {
            CartItem item1 = new CartItem
            {
                Product = new ProductEntity { Id = 1, Name = "Name1" },
                LogoFront = new LogoEntity { Id = 1 },
                Size = new SizeEntity { Id = 1, Size = "S" }
            };
            CartItem item2 = new CartItem
            {
                Product = new ProductEntity { Id = 2, Name = "Name2" },
                LogoFront = new LogoEntity { Id = 2 },
                Size = new SizeEntity { Id = 2, Size = "L" }
            };
            CartItem item3 = new CartItem
            {
                Product = new ProductEntity { Id = 3, Name = "Name3" },
                LogoFront = new LogoEntity { Id = 3 },
                Size = new SizeEntity { Id = 3, Size = "M" }
            };
            Cart cart = new Cart();
            cart.AddItem(item1.Product, item1.Size, item1.LogoFront, 0, null, 0, 1);
            cart.AddItem(item2.Product, item1.Size, item1.LogoFront, 0, null, 0, 1);
            cart.AddItem(item3.Product, item1.Size, item1.LogoFront, 0, null, 0, 1);
            cart.AddItem(item3.Product, item1.Size, item1.LogoFront, 0, null, 0, 3);
            cart.RemoveItem(item2.Product.Id, item2.Size.Id);
            Assert.AreEqual(cart.Items.Where(c => c.Product == item2.Product && c.Size == item2.Size).Count(), 0);
            Assert.AreEqual(cart.Items.Count(), 3);
        }

        [TestMethod]
        public void Test_CalcTotalPrice()
        {
            CartItem item1 = new CartItem
            {
                Product = new ProductEntity { Id = 1, Name = "Name1", Price = 10 },
                LogoFront = new LogoEntity { Id = 1 },
                Size = new SizeEntity { Id = 1, Size = "S" }
            };
            CartItem item2 = new CartItem
            {
                Product = new ProductEntity { Id = 2, Name = "Name2", Price = 20 },
                LogoFront = new LogoEntity { Id = 2 },
                Size = new SizeEntity { Id = 2, Size = "L" }
            };
            Cart cart = new Cart();
            cart.AddItem(item1.Product, item1.Size, item1.LogoFront, 0, null, 0, 1);
            cart.AddItem(item2.Product, item2.Size, item1.LogoFront, 0, null, 0, 1);
            cart.AddItem(item1.Product, item1.Size, item1.LogoFront, 0, null, 0, 3);
            decimal result = cart.CalcTotalPrice();
            Assert.AreEqual(result, 60);
        }

        [TestMethod]
        public void Test_Clear()
        {
            CartItem item1 = new CartItem
            {
                Product = new ProductEntity { Id = 1, Name = "Name1" },
                LogoFront = new LogoEntity { Id = 1 },
                Size = new SizeEntity { Id = 1, Size = "S" }
            };
            CartItem item2 = new CartItem
            {
                Product = new ProductEntity { Id = 2, Name = "Name2" },
                LogoFront = new LogoEntity { Id = 2 },
                Size = new SizeEntity { Id = 2, Size = "L" }
            };
            Cart cart = new Cart();
            cart.AddItem(item1.Product, item1.Size, item1.LogoFront, 0, null, 0, 1);
            cart.AddItem(item2.Product, item1.Size, item1.LogoFront, 0, null, 0, 1);           
            cart.Clear();
            Assert.AreEqual(cart.Items.Count(), 0);

        }
    }
}