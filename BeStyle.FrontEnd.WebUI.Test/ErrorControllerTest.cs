﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BeStyle.FrontEnd.WebUI.Models;
using BeStyle.Entities;
using BeStyle.FrontEnd.WebUI.Controllers;
using System.Web.Mvc;
using System.Web;
using System.IO;
using System.Web.Routing;

namespace BeStyle.FrontEnd.WebUI.Test
{
    [TestClass]
    public class ErrorControllerTest
    {
        [TestMethod]
        public void Http500Test()
        {
            var controller = new ErrorController();
            var request = new HttpRequest("", "http://example.com/", "");
            var response = new HttpResponse(TextWriter.Null);
            var httpContext = new HttpContextWrapper(new HttpContext(request, response));
            controller.ControllerContext = new ControllerContext(httpContext, new RouteData(), controller);
            var result = controller.Http500();
            Assert.IsNotNull(result);
            Assert.AreEqual(500, response.StatusCode);
        }
        [TestMethod]
        public void Http404Test()
        {
            var controller = new ErrorController();
            var request = new HttpRequest("", "http://example.com/", "");
            var response = new HttpResponse(TextWriter.Null);
            var httpContext = new HttpContextWrapper(new HttpContext(request, response));
            controller.ControllerContext = new ControllerContext(httpContext, new RouteData(), controller);
            var result = controller.Http404();
            Assert.IsNotNull(result);
            Assert.AreEqual(404, response.StatusCode);
        }
        [TestMethod]
        public void Http500AjaxTest()
        {
            var controller = new ErrorController();
            var request = new HttpRequest("", "http://example.com/", "");
            var response = new HttpResponse(TextWriter.Null);
            var httpContext = new HttpContextWrapper(new HttpContext(request, response));
            controller.ControllerContext = new ControllerContext(httpContext, new RouteData(), controller);
            var result = controller.Http500Ajax();
            Assert.IsNotNull(result);
            Assert.AreEqual(500, response.StatusCode);
            Assert.IsInstanceOfType(result, typeof(PartialViewResult));
        }
    }
}
