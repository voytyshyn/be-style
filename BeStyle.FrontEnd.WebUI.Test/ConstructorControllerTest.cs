﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BeStyle.FrontEnd.WebUI.Controllers;
using Moq;
using BeStyle.Repositories.Abstract;
using BeStyle.Entities;
using System.Collections.Generic;
using BeStyle.WebUI.Security;
using BeStyle.FrontEnd.WebUI.Models;
using System.Web.Mvc;
using System.Web;
using System.Web.SessionState;

namespace BeStyle.FrontEnd.WebUI.Test
{
    [TestClass]
    public class ConstructorControllerTest
    {
        [TestMethod]
        public void Test_Constructor_Index()
        {
            var mockProducts = new Mock<IProductRepository>();
            mockProducts.Setup(a => a.GetProductsAll()).Returns(new List<ProductEntity>());
            var mockSizes = new Mock<ISizeRepository>();
            mockSizes.Setup(a => a.GetSizes()).Returns(new List<SizeEntity>());
            var mockCategories = new Mock<ICategoryRepository>();
            mockCategories.Setup(a => a.GetAllCategory()).Returns(new List<CategoryEntity>());
            var mockLogos = new Mock<ILogoRepository>();
            mockLogos.Setup(a => a.GetAllLogos()).Returns(new List<LogoEntity>());
            var mockSecurity = new Mock<ISecurityManager>();
            mockSecurity.SetupGet(a => a.IsAuthenticated).Returns(true);
            mockSecurity.SetupGet(a => a.User).Returns(new UserInfo { UserId = 1, UserName = "Vitaly" });

            ConstructorController controller = new ConstructorController(
                mockProducts.Object,
                mockSizes.Object,
                mockCategories.Object,
                mockSecurity.Object,
                mockLogos.Object);

            ProductEntity product = new ProductEntity
            {
                Id = 1,
                Name = "Adidas"
            };

            var contextMock = new Mock<ControllerContext>();
            var mockHttpContext = new Mock<HttpContextBase>();
            var session = new Mock<HttpSessionStateBase>();
            mockHttpContext.Setup(ctx => ctx.Session).Returns(session.Object);
            contextMock.Setup(ctx => ctx.HttpContext).Returns(mockHttpContext.Object);
            controller.ControllerContext = contextMock.Object;
            var result = controller.Index() as ViewResult;
            Assert.IsNotNull(result.Model);
        }
    }
}
