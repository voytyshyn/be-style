﻿namespace BeStyle.WebUI.Security
{
    public interface ISecurityManager
    {
        bool Login(string userName, string password, bool rememberPass);
        void Logout();
        bool IsInRole(string role);
        void UpdateLogin(string newLogin);
        bool IsAuthenticated { get; }
        UserInfo User { get; }
        string CryptPassword(string password);

    }
}
